import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class CutPaste {
	
	public static void main (String args []) throws IOException{
		Scanner scanner = new Scanner(System.in);
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			
			System.out.println("Masukkan lokasi file .txt yang akan di cut");
			System.out.println("Contoh: ('D:/java/test/')");
			System.out.println("Jika file .txt berada pada folder yang sama dengan program, tulis 'same'");
			String InSource = scanner.next();
			String OutSource;
			
			if (InSource.contentEquals("same")){
				InSource = "";
			}
			
			System.out.println("Masukkan nama file .txt");
			System.out.println("Contoh: 'teks.txt'");
			String FileName = scanner.next();
			File source = new File(InSource + FileName);
			do{
				System.out.println("Masukkan lokasi file .txt yang akan di paste");
				System.out.println("Contoh: ('E:/Data/')");
				OutSource = scanner.next();
				
				if(OutSource.contentEquals(InSource)){
					System.out.println("Masukkan lokasi yang berbeda");
					continue;
				}
				break;
				
			}while(true);
			
		
			
			in = new FileInputStream(InSource + FileName);
			out = new FileOutputStream(OutSource + FileName);
			int c;
			
			while ((c = in.read()) != -1) {		//-1 means end of file
				out.write(c);
			}
			source.deleteOnExit();
			System.out.println(FileName + " Berhasil di cut ke " + OutSource);
			}
			catch(FileNotFoundException e){
				System.out.println("File tidak ditemukan");
			}
			finally {
				if (in != null) {
				in.close();
				}
				if (out != null) {
				out.close();
				}
			}
		scanner.nextLine();
	}
}

