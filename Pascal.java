//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.util.InputMismatchException;
//import java.util.Scanner;
public class Pascal {
	public static void main(String args []) throws IOException{
	//void Pascal() throws IOException{
		FileInputStream in = null;
		FileOutputStream out = null;
		
		try{
			in = new FileInputStream("PASCALInput.txt");
			out = new FileOutputStream("PASCALOutput.txt");
			int Row;
			int Reader;
			String string = "";
			while((Reader = in.read()) !=-1){
				if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				string = string + String.valueOf((char)Reader);
				
				if((Reader=in.read()) !=-1){
					
					string = string + String.valueOf((char)Reader);
				}
				
			}
			Row =Integer.valueOf(string);
			
			String newLine = System.getProperty("line.separator");
			
			int cRow, cNumber;
      
	        for (cRow = 0; cRow < Row; cRow++) {				
	        	
		            int spaces =(Row - cRow);					
		          
		            for (int index = 0; index < spaces; index++){
		            	string="   ";
		            	for(int Index = 0; Index < string.length(); Index++){
		    	        	out.write(string.charAt(Index));
		    	        }	
		    		}		              
		            long number = 1;									            
		            for (cNumber = 0; cNumber <= cRow; cNumber++){	                
		                if(number < 10){
		                	string = "  " + number + "   ";
		                	for(int Index = 0; Index < string.length(); Index++){
			    	        	out.write(string.charAt(Index));
			    	        }
		                		 	 
		                }else if(number < 100){
		                	string = ("  " + number + "  "); 		 
		                	for(int Index = 0; Index < string.length(); Index++){
			    	        	out.write(string.charAt(Index));
			    	        }
		                }else if(number < 1000){
		                	string = (" " + number + "  ");  		
		                	for(int Index = 0; Index < string.length(); Index++){
			    	        	out.write(string.charAt(Index));
			    	        }
		                }
		                else{
		                	string = (" " + number + " ");  		 	
		                	for(int Index = 0; Index < string.length(); Index++){
			    	        	out.write(string.charAt(Index));
			    	        }
		                }
		                number = number * (cRow - cNumber) / (cNumber + 1);			
		            }
		            string = newLine;
		            for(int Index = 0; Index < string.length(); Index++){
	    	        	out.write(string.charAt(Index));
	    	        }
	       }
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini memerlukan file PASCALInput.txt (dengan nilai variabel untuk baris) untuk dieksekusi");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("File PASCALInput.txt tidak memiliki nilai");
			System.out.println("Ubah nilai di file PASCALInput.txt");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("The pascalInput.txt tidak memiliki nilai");
			System.out.println("Ubah nilai di file PASCALInput.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}
	
	

}
