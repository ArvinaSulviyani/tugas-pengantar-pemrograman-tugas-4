import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class PermutasiKombinasi {
	public static void main (String args [])throws IOException {
	Scanner scanner = new Scanner(System.in);

		int Select =-1;
		do{
			System.out.println("Main Menu");
			System.out.println("1. Permutasi");
			System.out.println("2. Kombinasi");
			System.out.println("0. Exit");
			do{
				try{
					System.out.print("Masukkan pilihan : ");
					Select = scanner.nextInt();
					break;
					}
					catch(InputMismatchException e){
						System.out.println("Inputan salah");
						scanner.nextLine();
					}
			}while(true);
			switch(Select){
			case 1:
				permutasi();
				break;
			case 2:
				kombinasi();
				break;
			case 0:
				break;
			}
		}while(Select!=0);
	}	
	
	
	static void permutasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("NILAI.txt");
			System.out.println("-------------------------");
			System.out.println("Permutation");
			int N = 0, R = 0, NR;
			
			String string = "";
			int Reader;
			
			do{
				Reader = (char)in.read();
				
				if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				if (Reader ==' ')
				break;
				string = string + String.valueOf((char)Reader);
			}while(true);
		
			N =Integer.valueOf(string);
			System.out.println("Nilai N : "+N);
			string = "";
			
			
			while((Reader = in.read()) != -1){
				string = string + String.valueOf((char)Reader);
				if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				Reader=(char)in.read();			
			}
			
			R = Integer.valueOf(string);
			System.out.println("Nilai R : "+R);
			if(N<R){
				throw new Exception();
			}
			int Ntotal = 1;
			int NRtotal = 1;
			
			
			NR = N - R;
			for(int Index = 1; Index <= N; Index++){
				Ntotal *= Index;
			}
			for(int Index = 1; Index <= NR; Index++){
				NRtotal *= Index;
			}
			System.out.println("Permutasi N dan R : "+(Ntotal/NRtotal));
			System.out.println("-------------------------");
			}catch(FileNotFoundException e){
				System.out.println("\nFile tidak ditemukan");
				System.out.println("Program ini memerlukan file NILAI.txt (dengan 2 nilai variabel) untuk dieksekusi");
				System.out.println("-------------------------");
			}catch(IOException e){
				System.out.println("Error.");
				System.out.println("File NILAI.txt tidak memiliki 2 nilai (variabel dipisahkan dengan spasi dan harus bilangan positif)");
				System.out.println("-------------------------");
			}catch(NumberFormatException e){
				System.out.println("Error.");
				System.out.println("File NILAI.txt tidak memiliki 2 nilai (variabel dipisahkan dengan spasi dan harus bilangan positif) ");
				System.out.println("-------------------------");
			}catch(Exception e){
				System.out.println("Nilai R lebih kecil dari nilai N");
				System.out.println("Ubah nilai variabel di file NILAI.txt");
				System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
	
	
	static void kombinasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("NILAI.txt");
			System.out.println("-------------------------");
		System.out.println("Kombinasi");
		int N, R, NR;
		String string = "";
		int Reader;
		
		do{
			Reader = (char)in.read();
			if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
				throw new IOException();
			}
			if (Reader == ' ')
			break;
			string = string + String.valueOf((char)Reader);
		}while(true);
	
		N = Integer.valueOf(string);
		System.out.println("Nilai N : " + N);
		string = "";
		
		
		while((Reader = in.read()) != -1){
			string = string + String.valueOf((char)Reader);
			if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
				throw new IOException();
			}
			Reader=(char)in.read();	
		}
		
		R = Integer.valueOf(string);
		System.out.println("Nilai R : "+R);
		if(N < R){
			throw new Exception();
		}
		int Ntotal = 1;
		int Rtotal = 1;
		int NRtotal = 1;
		
		NR = N - R;
		for(int Index = 1; Index <= N; Index++){
			Ntotal *= Index;
		}
		for(int Index = 1; Index <= R; Index++){
			Rtotal *= Index;
		}
		for(int Index = 1; Index <= NR; Index++){
			NRtotal *= Index;
		}
		System.out.println("Kombinasi N dan R : "+(Ntotal/(Rtotal*NRtotal)));
		System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini memerlukan file NILAI.txt (dengan 2 nilai variabel) untuk dieksekusi");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("File NILAI.txt tidak memiliki 2 nilai (variabel dipisahkan dengan spasi dan harus bilangan positif)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("File NILAI.txt tidak memiliki 2 nilai (variabel dipisahkan dengan spasi dan harus bilangan positif)");
			System.out.println("-------------------------");
		}catch(Exception e){
			System.out.println("Nilai R lebih kecil dari nilai N");
			System.out.println("Ubah nilai variabel di file NILAI.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
}
