import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
public class Parabola{
public static void main (String args []) throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("Parabola.txt");
			System.out.println("-------------------------");
			System.out.println("Parabola");
			int a = 0, b = 0, c = 0;
			
			String string = "";
			int Reader;
			boolean negative = false;
				
			do{
				Reader = (char)in.read();
				if (Reader == 45) negative = true;		
				else if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				if (Reader ==' ')
				break;
				if(Reader!= 45) string = string + String.valueOf((char)Reader);
			}while(true);
		
			a = Integer.valueOf(string);
			if(negative) a = -a;
			negative = false;
			

			string = "";
			
			
			
			do{
				Reader = (char)in.read();
				if (Reader == 45) negative = true;		
				else if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				if (Reader ==' ')
				break;
				if(Reader != 45) string = string + String.valueOf((char)Reader);
			}while(true);
		
			b = Integer.valueOf(string);
			if(negative) b = -b;
			negative = false;
			
		
			string = "";
			
			if ((Reader = in.read()) == (-1)) System.out.println("Akhiri file");
			
			while(true){
				
				
				if (Reader == 45) negative = true;		
				else if((Reader > 57 && Reader != 32 && Reader != -1) || (Reader < 48 && Reader != 32 && Reader != -1)){
					throw new IOException();
				}
				if(Reader != 45) string = string + String.valueOf((char)Reader);
				if ((Reader = in.read()) == -1)
				break;
				else continue;
				
			}
			
			c = Integer.valueOf(string);
			if(negative) c = -c;
			negative = false;
			
			
			int D = (b * b) - (4 * a * c);
			int TitikBalikX = (-b / (2 * a));
			int TitikBalikY = D / (-4 * a);
			System.out.println(a + "x ^ 2 + " + b + "x + " + c);
			System.out.println("Titik balik (" + TitikBalikX + "," + TitikBalikY + ")");
			
			if (a > 0){
				System.out.println("Grafik Terbuka Ke Atas, Titik Balik Minimum");
			}
			else if (a < 0){
				System.out.println("Grafik Terbuka Ke Bawah, Titik Balik Maksimum");
			}
			
			if (TitikBalikX < 0) 
			System.out.println("Titik Balik Terletak Di kiri Sumbu Y");
			else if (TitikBalikX > 0) 
			System.out.println("Titik Balik Terletak Di kanan Sumbu Y");
			else 
			System.out.println("Titik Balik Terletak Di Sumbu Y");
			
			if (c < 0) 
			System.out.println("Grafik Memotong Sumbu Y Di bawah Sumbu X");
			else if (c > 0) 
			System.out.println("Grafik Memotong Sumbu Y Di atas Sumbu X");
			else 
			System.out.println("Grafik Memotong Sumbu Y Sumbu X");
			
			
			
			System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan Parabola.txt (dengan 3 variabel) untuk dieksekusi");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("File Parabola.txt tidak memiliki 3 variabel (dipisahkan dengan spasi)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("File Parabola.txt tidak memiliki 3 variabel (dipisahkan dengan spasi)");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
}